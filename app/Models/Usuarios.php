<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Usuarios extends Model
{
    use HasFactory, Notifiable, SoftDeletes;

    public $timestamps = false;

    protected $fillable = ['name', 'email'];
}
