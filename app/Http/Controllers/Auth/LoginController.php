<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function postLogin(Request $request)
    {

        $this->validate($request, [
        'email' => 'required|email',
        ]);

        $field = filter_var($request['email'], FILTER_VALIDATE_EMAIL) ? 'email' : 'name';
        $request->merge([ $field => $request['email'] ]);

        $credentials = $request->only($field);

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('welcome');
        }

    }


}
