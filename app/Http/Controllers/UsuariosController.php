<?php

namespace App\Http\Controllers;

Use App\Models\Usuarios;
use Illuminate\Http\Request;

class UsuariosController extends Controller
{
    private $success = 'success';
    private $title   = 'title';
    private $data    = 'data';
    private $error   = 'error';
    /**
     * Buscar registros
    */
    public function index()
    {
        try{
            $usuarios = Usuarios::all();

            if(!empty($usuarios)){

                $response = [
                    $this->success => 'ok',
                    $this->title   => 'Registrado con éxito',
                    $this->data    => $usuarios
                ];
            }

        }catch(Exception $e){

            $response = [
                $this->success => 'false',
                $this->title   => 'registro Fallido',
                $this->error   => $e
            ];
        }

        return response()->json($response);
    }

     /**
     * Buscar usuarios ID
    */
    public function show(Request $request, Usuarios $id)
    {
    
        try{
    
            $usuarios = Usuarios::find($id);
    
            if(!empty($usuarios)){
                $usuarios->update($request->all());
    
                $response = [
                    $this->success => 'ok',
                    $this->title   => 'Búsqueda con éxito',
                    $this->data    => $usuarios
                ];
            }
    
        }catch(Exception $e){
            $response = [
                $this->success => 'false',
                $this->title   => 'Búsqueda Fallida',
                $this->error   => $e
            ];
        }

        return response()->json($response);
    }

    /**
     * Crear usuarios
    */
    public function store(Request $request)
    {
        
        try{
            $dataTotal  = $request->getContent();
            $name       = $request->get('name');
            $email      = $request->get('email');

            if(isset($name) || isset($email) ){

                $registrarUsuario = new Usuarios();
                $registrarUsuario->name  = $name;
                $registrarUsuario->email = $email;
                $registrarUsuario->save();

                $response = [
                    $this->success => 'ok',
                    $this->title   => 'Registrado con éxito',
                    $this->data    =>  json_decode($dataTotal)
                ];
            }

        }catch(Exception $e){

            $response = [
                $this->success => 'false',
                $this->title   => 'registro Fallido',
                $this->error   => $e
            ];
        }

        return response()->json($response);
    }

    /**
     * Actualizar usuarios
    */
    public function update(Request $request, Usuarios $id)
    {
    
        try{
    
            $usuarios = Usuarios::findOrFail($id);
    
            if(!empty($usuarios)){
                $usuarios->update($request->all());
    
                $response = [
                    $this->success => 'ok',
                    $this->title   => 'Actualizado con éxito',
                    $this->data    => $usuarios
                ];
            }
    
        }catch(Exception $e){
            $response = [
                $this->success => 'false',
                $this->title   => 'Actualización Fallida',
                $this->error   => $e
            ];
        }

        return response()->json($response);
    }

    /**
     * Eliminar usuarios
    */
    public function delete(Usuarios $id)
    {
        try{

            $usuarios = Usuarios::find($id)->delete();
    
            if(!empty($usuarios)){
                $response = [
                    $this->success => 'ok',
                    $this->title   => 'Actualizado con éxito',
                    $this->data    =>  "Eliminado, usuario con el id:'$id'"
                ];
            }
    
        }catch(Exception $e){
            $response = [
                $this->success => 'false',
                $this->title   => 'Actualización Fallida',
                $this->error   => $e
            ];
        }
    
        return response()->json($response);
    }
}
