<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
Use App\Models\Usuarios;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Rutas API CRUD 

//Búsqueda Total de Usuarios
Route::get('usuarios', function() {
    
    $success = 'false';
    $title   = 'Búqueda Fallida';

    try{

        $usuarios = Usuarios::all();

        if(!empty($usuarios)){

            $response = [
                'success' => 'ok',
                'title'   => 'Búqueda General con éxito',
                'data'    =>  $usuarios
            ];
        }else{
            $response = [
                'success' => $success,
                'title'   => $title,
                'error'   => 'Falla en la búsqueda'
            ];

        }

    }catch(Exception $e){
        $response = [
            'success' => $success,
            'title'   => $title,
            'error'   => $e
        ];
    }

    return response()->json($response);

});

//Búsqueda por Id
Route::get('usuarios/{id}', function($id) {

    $success = 'false';
    $title   = 'Búqueda Fallida';

    try{

        $usuarios = Usuarios::find($id);

        if(!empty($usuarios)){

            $response = [
                'success' => 'ok',
                'title'   => 'Búqueda con éxito',
                'data'    =>  $usuarios
            ];
        }else{
            $response = [
                'success' => $success,
                'title'   => $title,
                'error'   => 'No se encontró registro'
            ];

        }

    }catch(Exception $e){
        $response = [
            'success' => $success,
            'title'   => $title,
            'error'   => $e
        ];
    }

    return response()->json($response);

});

//Registro de Usuarios
Route::post('usuarios', function(Request $request) {

$success = 'false';
$title   = 'registro Fallido';

    try{
        $data  = $request->getContent();
        $name  = $request->get('name');
        $email = $request->get('email');

        if(isset($name) || isset($email) ){

            $registrarUsuario = new Usuarios();
            $registrarUsuario->name  = $name;
            $registrarUsuario->email = $email;
            $registrarUsuario->save();

            $response = [
                'success' => 'ok',
                'title'   => 'Registrado con éxito',
                'data'    =>  json_decode($data)
            ];
        }

    }catch(Exception $e){

        $response = [
            'success' => $success,
            'title'   => $title,
            'error'   => $e
        ];
    }

    return response()->json($response);

});


//Actualización de usuarios
Route::put('usuarios/{id}', function(Request $request, $id) {

    $success = 'false';
    $title   = 'Actualización Fallida';

    try{

        $usuarios = Usuarios::findOrFail($id);

        if(!empty($usuarios)){
            $usuarios->update($request->all());

            $response = [
                'success' => 'ok',
                'title'   => 'Actualizado con éxito',
                'data'    =>  $usuarios
            ];
        }

    }catch(Exception $e){
        $response = [
            'success' => $success,
            'title'   => $title,
            'error'   => $e
        ];
    }

    return response()->json($response);

});


//Eliminar usuarios
Route::delete('usuarios/{id}', function($id) {
   
    $success = 'false';
    $title   = 'Eliminación Fallida';

    try{

        $usuarios = Usuarios::find($id)->delete();

        if(!empty($usuarios)){
            $response = [
                'success' => 'ok',
                'title'   => 'Eliminado con éxito',
                'data'    =>  "Eliminado, usuario con el id:'$id'"
            ];
        }

    }catch(Exception $e){
        $response = [
            'success' => $success,
            'title'   => $title,
            'error'   => $e
        ];
    }

    return response()->json($response);

});

//Búsqueda por Email para validar el login
Route::get('usuarios/email/{email}', function($email) {

    $success = 'false';
    $title   = 'Búqueda Fallida';

    try{

        $search      = Usuarios::where('email', $email)->first();
        $emailSearch = $search->email;
        
        if($email === $emailSearch){

            $response = [
                'success' => 'ok',
                'title'   => 'Búqueda con éxito',
                'data'    =>  $emailSearch
            ];
        }else{
            $response = [
                'success' => $success,
                'title'   => $title,
                'error'   => 'No se encontró registro'
            ];
        }

    }catch(Exception $e){
        $response = [
            'success' => $success,
            'title'   => $title,
            'error'   => $e
        ];
    }

    return response()->json($response);

});



/**
 * 
 *  Rutas para la vista por medio de CONTROLLER EN LARAVEL 8 VIENE POR DEFECTO
 * COMENTADA ÉSTA LINEA protected $namespace = 'App\\Http\\Controllers'; 
 * EN ROUTEsERVICEPROVIDER SOLO ES CUESTIÓN DE DESCOMENTARLA PARA ASÍ PODER USAR LOS CONTROLADORES
 * Y ÉSTAS RUTAS API
 * 
 * Route::get('usuarios', 'UsuariosController@index');
 * Route::get('usuarios/{usuario}', 'UsuariosController@show');
 * Route::post('usuarios', 'UsuariosController@store');
 * Route::put('usuarios/{usuario}', 'UsuariosController@update');
 * Route::delete('usuarios/{usuario}', 'UsuariosController@delete');
 * 
 * */


