-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 31-05-2021 a las 20:22:17
-- Versión del servidor: 10.4.19-MariaDB
-- Versión de PHP: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `registers`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2021_05_29_155958_create_usuarios_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `name`, `email`, `deleted_at`, `created_at`) VALUES
(1, 'Evalyn Stoltenberg', 'kaleb44@yahoo.com', NULL, NULL),
(2, 'Anthony Kshlerin', 'leffler.wade@bruen.com', NULL, NULL),
(3, 'Sharon Herzog III', 'monique.borer@durgan.biz', NULL, NULL),
(4, 'Prof. Anabelle Fritsch IV', 'omayer@hoppe.com', NULL, NULL),
(5, 'Fidel Dach', 'shanelle.koch@yahoo.com', NULL, NULL),
(6, 'Blake Nicolas DVM', 'rose.waelchi@hudson.com', NULL, NULL),
(7, 'Titus Casper Jr.', 'turner58@gmail.com', NULL, NULL),
(8, 'Dr. Brad Koss Sr.', 'haley.lurline@yahoo.com', NULL, NULL),
(9, 'Rasheed Reichel', 'nyasia.mueller@homenick.com', NULL, NULL),
(10, 'Dr. Nia Spinka', 'emery.johnson@yahoo.com', NULL, NULL),
(11, 'Adah Schimmel', 'pfeffer.garry@hotmail.com', NULL, NULL),
(12, 'Sigmund Shields IV', 'marlene32@hotmail.com', NULL, NULL),
(13, 'Mrs. Ellen Nader I', 'huels.henriette@yahoo.com', NULL, NULL),
(14, 'Filiberto Bradtke', 'koch.brisa@bergnaum.biz', NULL, NULL),
(15, 'Annalise Hirthe', 'jermaine.kautzer@yahoo.com', NULL, NULL),
(16, 'Dr. Heloise Dietrich Jr.', 'miracle.parisian@oreilly.com', NULL, NULL),
(17, 'Ms. Otha Cremin', 'crolfson@brekke.com', NULL, NULL),
(18, 'Kaci Cummerata', 'elizabeth.franecki@towne.org', NULL, NULL),
(19, 'Stephany Hegmann', 'shania31@hyatt.com', NULL, NULL),
(20, 'Beau Bahringer', 'emilia28@satterfield.org', NULL, NULL),
(21, 'Dixie Wuckert', 'stacey51@zulauf.net', NULL, NULL),
(22, 'Mrs. Callie Kovacek', 'ervin.gislason@pouros.net', NULL, NULL),
(23, 'Prof. Juvenal Moore', 'nhilpert@gmail.com', NULL, NULL),
(24, 'Zoey Mante', 'kuvalis.leonora@gmail.com', NULL, NULL),
(25, 'Dr. Abner Spencer DVM', 'abby.reichel@hotmail.com', NULL, NULL),
(26, 'Roderick Lebsack Jr.', 'albina.runte@yahoo.com', NULL, NULL),
(27, 'Martine Upton', 'dorthy.dooley@blanda.com', NULL, NULL),
(28, 'Miss Bettie Runte', 'marks.louvenia@roob.com', NULL, NULL),
(29, 'Dawn Zemlak', 'roxanne77@schaefer.com', NULL, NULL),
(30, 'Juliana Schmitt', 'major57@hotmail.com', NULL, NULL),
(31, 'Erik Simonis', 'schowalter.dejon@cruickshank.com', NULL, NULL),
(32, 'Rhett Christiansen', 'coralie.weissnat@mayert.info', NULL, NULL),
(33, 'Karen Considine', 'asporer@yahoo.com', NULL, NULL),
(34, 'Brian Waelchi', 'ygrant@hotmail.com', NULL, NULL),
(35, 'Ms. Berneice Gulgowski', 'gerlach.deborah@gmail.com', NULL, NULL),
(36, 'Caden Kemmer', 'hegmann.desiree@jerde.com', NULL, NULL),
(37, 'Hubert McGlynn II', 'frederique29@yahoo.com', NULL, NULL),
(38, 'Jayde Quitzon', 'ubaldo.bradtke@aufderhar.com', NULL, NULL),
(39, 'Prof. Dorian Cummings', 'julianne09@gleichner.org', NULL, NULL),
(40, 'Mathilde Sawayn', 'damion76@yahoo.com', NULL, NULL),
(41, 'Orlando Erdman', 'xwalker@hotmail.com', NULL, NULL),
(42, 'Enoch Lubowitz', 'cordell.breitenberg@jones.biz', NULL, NULL),
(43, 'Horace Torp', 'streich.mustafa@rosenbaum.com', NULL, NULL),
(44, 'Itzel O\'Conner', 'maxwell.little@bednar.com', NULL, NULL),
(45, 'Holden Gusikowski', 'woodrow57@lockman.com', NULL, NULL),
(46, 'Dr. Lesly Bartell II', 'cheyanne.wuckert@lakin.biz', NULL, NULL),
(47, 'Donald Simonis', 'joshua.tremblay@beier.info', NULL, NULL),
(48, 'Grady Howell', 'marisa.lakin@yahoo.com', NULL, NULL),
(49, 'Wilfred Jenkins', 'susana.bailey@konopelski.com', NULL, NULL),
(50, 'Dr. Jadon Wunsch PhD', 'torphy.sandra@keeling.com', NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usuarios_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
