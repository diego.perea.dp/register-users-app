# Registro de usuarios, con Login.

_Es un registro de usuarios y login básico "porque no cuenta con JWT u otro medio de seguridad, donde el back se realizó con Laravel y el front con ReactJs, En el back se crearon las Apis sin controladores, y con sus respectivas validaciones tanto en el back como en el front_
_Esto fue lo que se realizó en sí:_
_CRUD_
_Formulario de Creación y Edición_
_Listado de Registros_
_Sistemas Login y Borrado Lógico_
_Uso de Apis_
_TDD Básico_
_Seeders, Manejo de excepciones, separación de capas_
_Apis Documentadas en postman con ejemplos_
_Tecnologías usadas: Laravel, React, MySql_

## Iniciemos 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos 📋

_Tener instalado lo siguiente:_
_Composer 1.10_
_Node_
_Npm (en mi caso trabajé coon la Versión 14)_
_Mysql_


### Instalación 🔧

_Una serie de ejemplos paso a paso que te dice lo que debes ejecutar para tener un entorno de desarrollo ejecutandose_

_1. Clonar el proyecto_
_2. ingresar a la carperta "cd "nombre del proyecto"_
_3. ejecutar composer install_
_4. ejecutar npm install_
_5. conectar la base de datos en proyecto_
_6. realizar pruebas completas_

### Configuración 🔧

_Una serie de ejemplos paso a paso que te dice lo que debes ejecutar para tener un entorno de desarrollo ejecutandose_

_1. Después de realizar la instalación se explica un poco la configuración_
_2. Establecer conexión con su BD local"_

![Conexión en mi caso uso el puerto 3406, puedes usar el 3306 por defecto]
https://gitlab.com/diego.perea.dp/register-users-app/-/blob/main/img-git/Crear%20conexion%20.ENV

_3. Ejecutar la migración una vez se tenga la BD establecida en la configuración_
![$ php artisan migrate ]

_4. Ejecutar el listados de usuarios FAKE con seeders_
![$ php artisan db:seed --class=UsuariosTableSeeder]
https://gitlab.com/diego.perea.dp/register-users-app/-/blob/main/img-git/seeserd%20ejecucion


_5. APIS postman para ejecutar pruebas CRUD (GET, POST, DELETE, PUT)_
![Documentación Postman ]
https://gitlab.com/diego.perea.dp/register-users-app/-/blob/main/img-git/postmanApi

https://gitlab.com/diego.perea.dp/register-users-app/-/blob/main/Registro.registerusers_collection.json



_6. video de la ejecutación desde el front_
![Video Ejemplo ]

_https://www.loom.com/share/8d5cbeb59bb24448930a5067441227c5_


_Es un CRUD básico en el sentido que está abierto a modificaciones y aún le falta reforzar las validaciones_


## Construido con 🛠️

_Laravel, ReactJs, y una que otras librerías que se ven instaladas en el package.json_

## Ejemplo de ejecución

_https://www.loom.com/share/8d5cbeb59bb24448930a5067441227c5_


## Autores ✒️

* **Diego Perea** 

## Licencia 📄

_Free_

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Da las gracias públicamente 🤓.
