<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class Registro extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testRegistro()
    {
        $response = $this->get('api/usuarios');

        $response->assertStatus(200)
                 ->assertJsonStructure([
                     'id','name', 'email'
                 ]);

    }
}
