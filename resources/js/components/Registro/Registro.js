import React, {useEffect, useState} from 'react';
import styled from 'styled-components';
import { Link, withRouter } from 'react-router-dom';
import api from '../Helpers/api';



const Container = styled.div`
    max-width:1000px;
    width:98%;
    margin:auto;
    margin-top:5rem;
    border-radius:10px;
    background:white;
    box-shadow:0 3px 10px rgba(0,0,0,.3);
`;


const Registro = () =>  {

    const [users, setUsers] = useState(null);

    const fetchUsersAll = () => {

        api.getAllUsers().then(response =>{

            const dataResponse = response.data.data;
            setUsers(dataResponse);
        })
    }

    useEffect(() => {

        fetchUsersAll()

    }, [])

    const renderUsers = () =>{
        if(!users){
            return (
                <tr>
                    <td colSpan="4">
                        Loading Users...
                    </td>
                </tr>
            );
        }

        if(users.length === 0){
            return (
                <tr>
                    <td colSpan="4">
                        There is no users... Add one
                    </td>
                </tr>
            );
        }

        //User.map para recorrer la base de datos de los usuarios 
        return users.map((user) => (
            <tr>
                <td>{user.id} </td>
                <td>{user.name} </td>
                <td>{user.email} </td>
                <td>
                    <Link 
                        className = "btn btn-primary"
                        to={`/actualizar/${user.id}`}
                        >Update
                    </Link>
                    <button 
                        className = "btn btn-danger"
                        onClick={()=> 
                            api.deleteUsers(user.id)
                            .then(fetchUsersAll)
                            .catch(error =>{
                                alert('No se logró eliminar: ,' + user.id)
                            })
                        
                        }
                        to={`/usuarios/${user.id}`}
                        > Delete
                    </button>
                </td>
            </tr>
        ))

    }

    return (

        <Container>

        <table className="table table-striped">
             <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Options</th>
                </tr>
            </thead>
            <tbody>
             {renderUsers()}
            </tbody>
            <Link 
                className = "btn btn-secondary"
                to={'/'}
                > Close
            </Link>

        </table>
        </Container>
    );
}

export default withRouter(React.memo(Registro));