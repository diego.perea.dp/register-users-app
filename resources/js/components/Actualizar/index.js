import React,{useState, useEffect} from 'react';
import { Link, withRouter, useHistory, useParams} from 'react-router-dom';
import Formulario from '../Formulario/Forms';
import api from '../Helpers/api';


//similar lógina y componente a Agregar usuarios
const Actualizar = (props) =>  {

    const { id}                 = useParams();
    const history               = useHistory();
    const [loading, setLoading] = useState(false);
    const [name, setName]       = useState('');
    const [email, setEmail]     = useState('');

    const onUpdateSumit = async () => {

        setLoading(true);

        try{
            const response = await api.updateUsers({
                name, email
            }, id)
            console.log(response)
            //Validar y redireccionar 
            if(response.data.success === 'ok'){

                history.push('/registro');
            }else{
                history.push('/registrar');
            }
           
        }catch {
            setLoading(false)
        }
    };

    useEffect(() =>{

        api.getOneUsers(id).then(response => {
            const dataResponse = response.data.data;
            setName(dataResponse.name);
            setEmail(dataResponse.email)
        })
    }, [])


    return (
        <Formulario
            title= {` Update - ${name}`}
        >
            <div class="mb-3">
                <label class="form-label">Name</label>
                <input 
                    type="text" 
                    class="form-control" 
                    value={name}
                    onChange={event => setName(event.target.value)}
                />
            </div>
            <div class="mb-3">
                <label class="form-label">Email</label>
                <input 
                    type="email"
                    class="form-control"
                    value={email}
                    onChange={event => setEmail(event.target.value)}
                />
            </div>
            <div class="card-footer  bg-light">
            <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                <div class="btn-group">
                    <Link 
                    class="btn btn-secondary btn-sm"
                    type="submit" 
                    to={'/'}
                    >Close</Link>
                </div>
                <div class="btn-group">
                    <button 
                    class="btn btn-primary btn-sm"
                    type="submit" 
                    onClick={onUpdateSumit}
                    disabled={loading}
                    >
                    {loading ? 'LOADING...' : 'Update'}
                    
                    </button>
                </div>
            </div>
            </div>


        </Formulario>
    );
}

export default withRouter(React.memo(Actualizar));