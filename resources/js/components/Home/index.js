import React from 'react';
import styled from 'styled-components';
import { Link, withRouter } from 'react-router-dom';
import history from 'history';

const Container = styled.div`
    max-width:500px;
    width:98%;
    margin:auto;
    margin-top:5rem;
    border-radius:10px;
    background:white;
    box-shadow:0 3px 10px rgba(0,0,0,.3);
`;


const Home = (props) =>  {
    
    return (

        <Container  className={"p-3 p-md-4"} >

            <div class="card">
                <div class="card-header">
                    Register Users
                </div>
                <div class="card-body">
                    <h5 class="card-title">CRUD</h5>
                    <li> Formulario de Creación y Edición </li>
                    <li> Listado de Registros </li>
                    <li> Sistemas Login y Borrado Lógico </li>
                    <li> Uso de Apis </li>
                    <li> TDD Básico </li>
                    <li> Seeders, Manejo de excepciones, separación de capas </li>
                    <li> Apis Documentadas en postman con ejemplos </li>
                    <li>Tecnologías usadas: Laravel, React, MySql </li>
                    <br></br>
                    <Link 
                        type="button" 
                        class="btn btn-primary"
                        to={'/ingreso'}
                    >Go 
                    </Link>
                </div>
            </div>

        </Container>


    );
}
export default withRouter(React.memo(Home));