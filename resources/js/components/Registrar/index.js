import React,{useState} from 'react';
import { Link, withRouter, useHistory} from 'react-router-dom';
import Formulario from '../Formulario/Forms';
import api from '../Helpers/api';

const Registrar = (props) =>  {

    const history = useHistory();
    const [loading, setLoading] = useState(false);
    const [name, setName]       = useState('');
    const [email, setEmail]     = useState('');

    const onRegisterSumit = async () => {

        setLoading(true);

        try{
            const response = await api.registerUsers({
                name, email
            })

            //Validar y redireccionar 
            if(response.data.success === 'ok'){

                history.push('/registro');
            }else{
                history.push('/registrar');
            }
           
        }catch {
            setLoading(false)
        }
    };
 
    return (
        <Formulario
            title= "New User"
        >
            <div class="mb-3">
                <label class="form-label">Name</label>
                <input 
                    type="text" 
                    class="form-control" 
                    value={name}
                    onChange={event => setName(event.target.value)}
                />
            </div>
            <div class="mb-3">
                <label class="form-label">Email</label>
                <input 
                    type="email" 
                    class="form-control" 
                    value={email}
                    onChange={event => setEmail(event.target.value)}
                />
            </div>
            <div class="card-footer  bg-light">
            <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                <div class="btn-group">
                    <Link 
                    class="btn btn-secondary btn-sm"
                    type="submit" 
                    to={'/'}
                    >Close</Link>
                </div>
                <div class="btn-group">
                    <button 
                    class="btn btn-primary btn-sm"
                    type="submit" 
                    onClick={onRegisterSumit}
                    disabled={loading}
                    >
                    {loading ? 'LOADING...' : 'Save'}
                    
                    </button>
                </div>
            </div>
            </div>


        </Formulario>
    );
}

export default withRouter(React.memo(Registrar));