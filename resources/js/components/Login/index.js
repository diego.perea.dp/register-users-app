import React, {useState} from 'react';
import { Link, withRouter, useHistory } from 'react-router-dom';
import Formulario from '../Formulario/Forms';
import api from '../Helpers/api';

const Login = (props) =>  {

    const history               = useHistory();
    const [loading, setLoading] = useState(false);
    const [email, setEmail]     = useState('');
    const [state, setState]     = useState(false);

    const onLoginSumit = async () => {
        setLoading(true);

        try{

            await api.getEmailLogin(email).then(response =>{

                const dataResponse = response.data;

                setEmail(dataResponse);
    
                if(dataResponse.success === 'ok'){
                    setState(true)
                    history.push('/registro')
                }else{
                    setState(false)
                    history.push('/')
                }
            })
           
        }catch {
            setLoading(false)
        }
    };


    return (

        <Formulario
        title= "Login User "
    >
            <div class="mb-3">
                <label class="form-label">Email</label>
                <input 
                    type="email" 
                    class="form-control" 
                    value={email}
                    onChange={event => setEmail(event.target.value)}
                />
                 {!!!state && loading
                        ? 
                        <div 
                            class="alert alert-danger" 
                            role="alert">
                                This is users no... 
                            <a class="alert-link">Register
                            </a>
                        </div> 
                    : 
                    '' 
                    }

            </div>
            <div class="card-footer  bg-light">
            <div class="d-grid gap-2 d-md-flex justify-content-md-end">
            <div class="btn-group">
                <button 
                    class="btn btn-primary btn-sm"
                    type="submit" 
                    onClick={onLoginSumit}
                    disabled={loading}
                    >
                    {loading ? 'LOADING...' : 'Login'}
                    
                </button>
                </div>
                <div class="btn-group">
                    <Link 
                    class="btn btn-secondary btn-sm"
                    type="submit" 
                    to={'/'}
                    >Close</Link>
                </div>
               
                <div class="btn-group">
                    <Link 
                    // class="btn btn-primary btn-sm"
                    type="submit" 
                    to={'/registrar'}
                    >Register..? </Link>
                </div>
            </div>
            </div>
    </Formulario>

    );
}

export default withRouter(React.memo(Login));