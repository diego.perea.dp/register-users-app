import axios from 'axios';

//Esta variable se puede agregar en el .ENV por seguridad, por razones de pruebas se deja por ahora acá
const BASE_URL_API = "http://localhost:8000/api";

//Rutas API para trabajar en el front


export default {
    getAllUsers: () => 
        axios.get(`${BASE_URL_API}/usuarios`),
    getOneUsers: (id) => 
        axios.get(`${BASE_URL_API}/usuarios/${id}`),
    registerUsers: (user) => 
        axios.post(`${BASE_URL_API}/usuarios`, user),
    updateUsers: (user, id) => 
        axios.put(`${BASE_URL_API}/usuarios/${id}`, user),
    deleteUsers: (id) => 
        axios.delete(`${BASE_URL_API}/usuarios/${id}`),
    getEmailLogin: (email) => 
        axios.get(`${BASE_URL_API}/usuarios/email/${email}`)
}