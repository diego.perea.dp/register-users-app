import React, {lazy} from 'react';
import ReactDOM from 'react-dom';
import { 
        BrowserRouter as Router, 
        Switch,
        Route
     } from 'react-router-dom';
     
import Home  from '../Home';
import Login from '../Login';
import Registro from '../Registro/Registro';
import Registrar from '../Registrar';
import Actualizar from '../Actualizar';

import 'bootstrap/dist/css/bootstrap.min.css';

class App extends React.Component {
    render() {
        return (
            <Router>
                <Switch>
                    <Route exact path={'/'} render={() => <Home/>}/>
                    <Route exact path={'/ingreso'} render={() => <Login/>}/>
                    <Route exact path={'/registro'} render={() => <Registro/>}/>
                    <Route exact path={'/actualizar/:id'} render={() => <Actualizar/>}/>
                    <Route exact path={'/registrar'} render={() => <Registrar/>}/>
                </Switch>
            </Router>
        )
    }
}

ReactDOM.render(
    <App />
    , document.getElementById('app'));

