import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
    max-width:500px;
    width:98%;
    margin:auto;
    margin-top:5rem;
    border-radius:10px;
    background:white;
    box-shadow:0 3px 10px rgba(0,0,0,.3);
`;


const Formulario = ({title, children}) =>  {
    return (

        <Container  className={"p-3 p-md-4"} >
            <div class = "container"> 
            <form class = "row g-3">
            <div class="card ">
                <div class="card-header bg-light ">
                    <label class="form-label d-grid gap-2 d-md-flex ">{title}</label>
                </div>
                <div class= "card-body">
                {children}
                </div>
            </div>
            </form>
            </div>
        </Container>
    );
}

export default React.memo(Formulario);