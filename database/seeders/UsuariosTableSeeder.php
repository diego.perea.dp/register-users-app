<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Usuarios;


class UsuariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncar los registros existentes para empezar de cero
        Usuarios::truncate();

        $faker = \Faker\Factory::create();

        //Crear los usuarios en la base de datos 
        for ($i = 0; $i < 50; $i++) {
            Usuarios::create([
                'name' => $faker->name,
                'email' => $faker->email,
            ]);
        }


    }
}
